window.onload = iniciar;

function iniciar() {
    document.getElementById("submitButton").addEventListener("click", validar, false);
}


function validaEmail() {
    let email = document.getElementById("emailAddress").value;
    if (email.toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )) {

        let terminacion = email.split('.');
        if (terminacion[1] == 'es' || terminacion[1] == 'net' || terminacion[1] == 'org' || terminacion[1] == 'com') {
            return true;
        }
    }
    document.getElementById("badEmail").style.display = "block";
    return false;
}

function validar(e) {
    borrarError();
    let okValidaEmail = validaEmail();
    if (okValidaEmail && confirm("Pulsa aceptar para mandar el formulario")) {
        return true;
    } else {
        e.preventDefault();
        return false;
    }
}

function borrarError() {
    document.getElementById("badEmail").style.display = "none";
}

